Source: filetea
Section: net
Priority: optional
Maintainer: Alberto Garcia <berto@igalia.com>
Build-Depends: debhelper-compat (= 13),
               uuid-dev,
               libevd-0.2-dev,
               libjson-glib-dev
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://github.com/elima/FileTea
Vcs-Browser: https://salsa.debian.org/berto/filetea
Vcs-Git: https://salsa.debian.org/berto/filetea.git

Package: filetea
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends},
         adduser,
         shared-mime-info,
         ${misc:Depends}
Suggests: ssl-cert
Description: Web-based file sharing system
 FileTea is a simple way to send files among different users.
 .
 FileTea functions as a web server. Users can drag files into their
 web browser and a URL will be generated for each one. Those URLs can
 be sent to other people, who will be able to start downloading the
 files immediately.
 .
 An HTML5 capable browser is required in order to share the files, but
 any HTTP client can download them, including command-line tools such
 as curl or wget.
 .
 Files are sent through the server, but no data is stored there:
 FileTea is only used to route the traffic. This also means that
 there's no limit to the size of shared files.
 .
 The service is anonymous and does not require user registration.
 Since it's completely web-based, it works in networks with proxies,
 firewalls, etc., as long as all users can reach the FileTea server.
